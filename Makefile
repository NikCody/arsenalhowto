OUTPUT_NAME = 'Arsenal'

all: pdf

pdf: $(OUTPUT_NAME).pdf

%.pdf: src/main.tex
	mkdir -p build
	rubber -f --pdf --into build --jobname $* -S --warn all $<

clean:
	rubber --pdf --into build --jobname $(OUTPUT_NAME) --clean src/main.tex
	rm -rf build

.PHONY: clean
