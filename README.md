# Introduction
A presentation written in LATEX about Android Arsenal, explaining how to share a library on the web site.
# Compile
In order to produce a document, get a valid XeLATEX compiler.
Even a makefile is distributed, employing Rubber build system for LATEX to call LATEX compiler as many times as required.
